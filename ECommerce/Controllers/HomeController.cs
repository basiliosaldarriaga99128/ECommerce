﻿using ECommerce.Models;
using System.Linq;
using System.Web.Mvc;



namespace ECommerce.Controllers
{
    public class HomeController : Controller
    {
        private ECommerceContext db = new ECommerceContext();

        public ActionResult Index()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            
            return View(user);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Distributed programming .";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "My data contact.";

            return View();
        }
    }
}